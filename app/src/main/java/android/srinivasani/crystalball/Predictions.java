package android.srinivasani.crystalball;

public class Predictions {

    private static Predictions predictions;
    private String[] answers;

    private Predictions() {
        answers = new String[]{
                "Your wishes will come true.",
                "Your wishes will NEVER come true.",
                "Try again later.",
                "All will go as you hoped",
                "Let's see how things go",
                "You are very lucky today",
        };


    }

    public static Predictions get() {
        if (predictions == null) {
            predictions = new Predictions();
        }
        return predictions;
    }

    public String getPrediction(int randomNumber) {
        return answers[randomNumber];
    }
}
















